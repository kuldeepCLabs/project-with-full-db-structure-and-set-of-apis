process.env.NODE_ENV='localDevelopment';
config=require('config');
dbConnection = require('./routes/dbConnection');
constant=require('./routes/constant');

var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http=require('http');


var routes = require('./routes/index');
var user = require('./routes/user');
var driver=require('./routes/driver');
var orderbook=require('./routes/order_book');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces',1);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/test', routes);
app.get('/changePassword',routes);

app.post('/signup_user',multipartMiddleware);
app.post('/signup_user',user);
app.post('/login_user',user);
app.post('/logout',user);
app.post('/forgetpassword',user);
app.post('/changepassword',user);
app.post('/changepasswordonetimelink',user);
app.post('/edit_profile',multipartMiddleware);
app.post('/edit_profile',user);


app.post('/signup_driver',multipartMiddleware);
app.post('/signup_driver',driver);
app.post('/verify_driver',driver);
app.post('/assign_driver',driver);
app.post('/accepted_notification',driver);
app.post('/start_service',driver);
app.post('/end_service',driver);
app.post('/start_service',driver);

app.post('/get_driver',orderbook);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
http.createServer(app).listen(config.get('PORT'),function(){
    console.log("Express Server is listening at the port :"+config.get('PORT'));
});

module.exports = app;
