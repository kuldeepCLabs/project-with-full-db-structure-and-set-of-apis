
var express = require('express');
var router = express.Router();

var sendResponse=require('./sendResponse');
var math=require('math');
var fs=require('fs');
var AWS=require('aws-sdk');
var s3=config.get('AWS_SETTINGS');
var request=require('request');
var nodemailer=require('nodemailer');
var autht=config.get('Email_Settings');
var passwordgenerator=require('password-generator');
var md5=require('MD5');
var geolib=require('geolib');
//var distance = require('google-distance');
// function used to generate the random file name
exports.generateRandomNumber=function() {
    var timestamp = new Date().getTime().toString();
    var str = '';
    var chars = "abcdefghijkulmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var size=chars.length;

    for (var i = 0; i < 4; i++) {
        var randno = math.floor(math.random() * size);
        str = chars[randno] + str;
    }
    return str+timestamp;
}

//check the blank value at any parameter
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == ''||arr[i] == undefined||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

// that function is used to encrypt the original text
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

//check the user is access token valid or inbvalid
exports.authenticateUser = function(res,userAccessToken, callback)
{
    var sql = "SELECT `id` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
    dbConnection.Query(res,sql, [userAccessToken], function(result) {
        console.log(result.length);
        if (result.length > 0) {
            return callback(result);
        } else {
            sendResponse.invalidAccessTokenError(res);
        }
    });
};
//check the admin access token valid or not
exports.authenticateAdmin = function(res,adminAccessToken, callback)
{
    var sql = "SELECT `id` FROM `tb_admin` WHERE `access_token`=? LIMIT 1";
    dbConnection.Query(res,sql, [adminAccessToken], function(result) {
        console.log(result.length);
        if (result.length > 0) {
            return callback(1);
        } else {
            sendResponse.invalidAccessTokenError(res);
        }
    });
};
//send the mail
exports.sendMail=function(email,name,res,callback) {


    var smtpTransport = nodemailer.createTransport("Direct", {debug: true}

    );

    var mailOptions = {
        from: autht.emailFrom,
        to:email,
        subject: "Successfully Registered",
        text: "Check the email for testing"+name
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            sendResponse.somethingWentWrongError(res);
        } else {
            return callback(1);
        }
    });

}

//check the email is already exist or not if not then insert the data
exports.checkEmailAvailability=function(res, email, callback1){

    var sql='select `id` from `tb_user` where `email`=? limit 1';
    var values=[email];
    console.log("email check");
    dbConnection.Query(res,sql,values,function(userResponse){

        console.log(userResponse.length+"jddsd");
        if(userResponse.length){
            console.log("already exists");
            sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_EXISTS,res);
        }
        else{
            checkEmailValidation(res,email,function(callback) {
                if (callback == null) {
                    return callback1(null);
                }
                else{
                    sendResponse.somethingWentWrongError(res);
                }
            });
        }
    });

}

//check the email domain name
function checkEmailValidation(ress,email,callback){

    request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: email},
        json: true
    }, function (err,res,body) {
        console.log(body.valid);
        if (body.valid){
            console.log("valid the emil domain checking");
           return  callback(null);
        }
        else {
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_EMAIL,ress);
        }

    });
}
//upload the image in database and s3 server
exports.uploadImage=function(res,imageName,callback){

    uploadImageToS3Bucket(imageName,'images',function(result_image){

        if(result_image===0){
            sendResponse.sendErrorMessage(constant.responseMessage.UPLOAD_IMAGE_ERR,res);
        }
        else{
            var s3_path = 'http://internsclabs.s3.amazonaws.com/images/'
            var path = s3_path + result_image;

            return callback(path);
        }
    });
}

function uploadImageToS3Bucket(file,folder,callback){
    var filename=file.name;
    var path=file.path;
    var mimetype=file.type;

    fs.readFile(path,function(err,file_buffer){
        if(err){
            return callback(0);
        }

        AWS.config.update({accessKeyId: s3.accessId, secretAccessKey: s3.secretKey});
        var s3bucket = new AWS.S3();
        var params = {Bucket: s3.bucketName, Key: folder + '/' + filename, Body: file_buffer, ACL: s3.acl1, ContentType: mimetype};

        s3bucket.putObject(params, function(err, data) {
            if (err){
                return callback(0);
            }
            else{
                return callback(filename);
            }
        });

    });
}
//check the email
exports.checkEmailRegistered=function(res,email,callback){
    sql1='select `id`,`email`,`address`,`access_token`,`password`,`reg_as`,`verified_status` from tb_user where `email`=? limit 1';
       console.log("email through the login user"+email);
       var values=[email];
    dbConnection.Query(res,sql1,values,function(resultEmail){
         console.log(resultEmail);
        if(resultEmail.length == 0){
            console.log("that email does not registered");
            sendResponse.sendErrorMessage(constant.responseMessage.NOT_REGISTERED,res);
        }
        else{
            console.log("email in database");
            return callback(resultEmail);
        }
    });
}

exports.sendMail2=function(res,to,sub,msg,callback) {


    var smtpTransport = nodemailer.createTransport("Direct", {debug: true}

    );

    var mailOptions = {
        from: autht.emailFrom,
        to:to,
        subject: msg,
        html: sub
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            sendResponse.somethingWentWrongError(res);
        } else {
            return callback(1);
        }
    });

}

exports.checkOneTimeLink=function(res,oneTimePassword,callback){
    sql1='select `id` from tb_user where one_time_password=? limit 1';
    console.log(oneTimePassword);
    dbConnection.Query(res,sql1,[oneTimePassword],function(resultId){
        console.log(resultId);
         if(resultId==0){
             sendResponse.sendErrorMessage(constant.responseMessage.EXPIRE_SESSION,res);
         }
        else{
             return callback(resultId);
         }
    });
}

exports.generateOneTimePassword=function(res,id,callback){
    var oneTimePassword=passwordgenerator(10,false);
    var encryptPassword=md5(oneTimePassword);

    console.log("one time password"+oneTimePassword+"id"+id);
    sql='update `tb_user` set `one_time_password`=? where id=?';

    dbConnection.Query(res,sql,[encryptPassword,id],function(result){
        //console.log("generate and update the one time password",result);
        console.log(result);
        var effectrrows = result.affectedRows;
        console.log(effectrrows);
        if(effectrrows==0){
            sendResponse.somethingWentWrongError(res);
        }
        else{
            return callback(encryptPassword);
        }
    });
};

exports.checkPasswordAvailable=function(req,res,id,oldpassword,newpassword){

        userId = id;
        console.log(userId);
        sql = 'select `password` from `tb_user` where `id`=? limit 1';
        dbConnection.Query(res, sql, [userId], function (resultPassword) {
            console.log("resultpassword"+resultPassword);
            if (resultPassword.length == 0) {
                console.log("does not return ay row into database with any access token");
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var encryptPassword = md5(oldpassword);
                var password1 = resultPassword[0].password;
                console.log(encryptPassword == password1);
                if (encryptPassword == password1) {
                    var newPassword = md5(newpassword);
                    sql = 'update `tb_user` set `password`=? where `id`=? limit 1';
                    dbConnection.Query(res, sql, [newPassword, userId], function (resultUpdatePassword) {
                        if (resultUpdatePassword.length == 0) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            sendResponse.successStatusMsg(constant.responseMessage.CHANGE_PASSWORD, res);
                        }
                    });
                }
                else {
                    console.log("Invaild password");
                    sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_PASSWORD, res);

                }
            }

    });
}

exports.getAutheriseDriver=function(currentLatitude,currentLongitude,centeralLatitude,centralLongitude,radius,callback){
    var value=geolib.isPointInCircle(
        {latitude: currentLatitude, longitude: currentLongitude},
        {latitude: centeralLatitude, longitude: centralLongitude},
        radius
    );
    return callback(value);
};


exports.checkdistance=function(lat1,long1,lat2,long2,callback){
    //console.log(lat1+long1+lat2+long2+math.PI);
    var radlat1 = math.PI * lat1/180;
    var radlat2 = math.PI * lat2/180;
    var radlong1 = math.PI * long1/180;
    var radlong2 = math.PI * long2/180;
    var theta = long1-long2;
    var radtheta = math.PI * theta/180;
    var dist = math.sin(radlat1) * math.sin(radlat2) + math.cos(radlat1) * math.cos(radlat2) * math.cos(radtheta);
    dist = math.acos(dist);
    dist = dist * 180/math.PI;
    dist = dist * 60 * 1.1515;
    //dist = dist * 1.609344;
    dist=dist*1000.0;   //convert the miles to meter
     console.log("dist"+dist);
    return callback(math.round(dist));

}

exports.sortValue = function(array, key) {
    //console.log(key);
   return array.sort(function(a, b) {
         console.log('a'+a,'b'+b);
        var x = a[key];
        var y = b[key];
         console.log('x'+x,'y'+y);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  //  return array;
};

exports.authenticateDriver=function(res,driverAccessToken,callback){
    var sql = "SELECT `id`,phonono,current_latitude,current_longitude,last_order_id,driver_request FROM `tb_user` WHERE `access_token`=? LIMIT 1";
    dbConnection.Query(res,sql, [driverAccessToken], function(result) {
        console.log(result.length);
        if (result.length > 0) {
            return callback(result);
        } else {
            sendResponse.invalidAccessTokenError(res);
        }
    });
};